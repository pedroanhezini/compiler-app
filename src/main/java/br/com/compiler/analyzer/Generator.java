package br.com.compiler.analyzer;

import java.io.File;
import java.nio.file.Paths;
import jflex.Main;

public class Generator {

	public void generate() {
		String rootPath = Paths.get("").toAbsolutePath().toString();
		String subPath = "/src/main/java/br/com/compiler/";
		String file = rootPath + subPath + "language.lex";
		File sourceCode = new File(file);
		Main.generate(sourceCode);
	}
}
